__Dependências__

- __[NodeJS](https://nodejs.org/en/download/)__ - Interpretador JS.

- __[Ionic 3](https://ionicframework.com/getting-started)__ - Framework.

---

__Comandos__

    npm install

    ionic serve

---
__Link do Projeto (Front-End):__

- __[CastleMonster](https://castle-monster.firebaseapp.com/)__ - https://castle-monster.firebaseapp.com.
---