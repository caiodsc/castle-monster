import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { PlayPage} from '../pages/play/play';
import { RankingPage } from '../pages/ranking/ranking';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {GamePage} from "../pages/game/game";
import { HttpClientModule} from "@angular/common/http";
import {GamePageModule} from "../pages/game/game.module";

export const APIUrl = "https://castlemonster.herokuapp.com";

@NgModule({
  declarations: [
    MyApp,
    PlayPage,
    RankingPage,
    HomePage,
    TabsPage
  ],
  imports: [
    GamePageModule,
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(MyApp, {tabsHideOnSubPages: true})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PlayPage,
    RankingPage,
    HomePage,
    TabsPage,
    GamePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
