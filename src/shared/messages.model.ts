export class Log {
  text: string;
  side: string;
  type: string;

  constructor(text: string, side: string, type: string){
    this.text = text;
    this.side = side;
    this.type = type;
  }

}
