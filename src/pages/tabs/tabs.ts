import { Component } from '@angular/core';

import { PlayPage } from '../play/play';
import { RankingPage } from '../ranking/ranking';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = PlayPage;
  tab3Root = RankingPage;

  constructor() {

  }
}

