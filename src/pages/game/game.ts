import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import {Log} from "../../shared/messages.model";
import {APIUrl} from "../../app/app.module";

@IonicPage()
@Component({
  selector: 'page-game',
  templateUrl: 'game.html',
})
export class GamePage {
  hpwarrior = 100;
  hpmonster = 100;
  data: any;
  logs = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public alertCtrl: AlertController) {
    this.data = navParams.get("data");
  }



  atacar(type) {
   this.http.post(`${APIUrl}/play`, JSON.stringify({
      type: type,
      name: this.data.name
    }), {headers: {'Content-Type': 'application/json'}})
      .subscribe(data => {
        console.log(data);
        this.data = data;
        let hpm = document.getElementById("monstergif");
        let hpw = document.getElementById("warriorgif");
        hpm.setAttribute('src', "../../assets/gifs/mattack.gif");
        let that = this;
        if (data["life"] - data["cure"] > 0) {
          setTimeout(function () {
            if(data["monsterAttack"] > 0){
              that.logs.unshift(new Log(`Monstro causou dano (-${data["monsterAttack"]})`, "right", "danger"));
            } else {
              that.logs.unshift(new Log(`Monstro não causou dano`, "right", "danger"));
            }
            that.hpwarrior -= data["monsterAttack"];
            hpm.setAttribute('src', '../../assets/gifs/monster.gif');
            setTimeout(function () {
              hpw.setAttribute('src', '../../assets/gifs/attack.gif');
            }, 1500);
            setTimeout(function () {
              that.hpwarrior += data["cure"];
              that.hpmonster = data["monsterLife"];
              if (type == "ataque") {
                that.logs.unshift(new Log(`Jogador atacou o monstro (-${data["attack"]})`, "left", "primary"));
              } else {
                that.logs.unshift(new Log(`Jogador usou o golpe especial (+${data["attack"]})`, "left", "secondary"));
              }
              if (data["cure"] > 0){
                that.logs.unshift(new Log(`Jogador curou-se durante o especial (+${data["cure"]})`, "left", "golden"));
              }
              hpw.setAttribute('src', '../../assets/gifs/warrior.gif');
              if (that.hpmonster == 0) {
                hpm.setAttribute('src', '../../assets/gifs/mdeath.gif');
                setTimeout(function () {
                  const alert = that.alertCtrl.create({
                    enableBackdropDismiss: false,
                    title: 'Vitória!',
                    message: `Você conseguiu derrotar o monstro! Pontuação: ${(data["life"] * 1000 / data["round"]).toFixed(0)}px.`,
                    buttons: [{
                      text: 'OK',
                      handler: () => {
                        that.navCtrl.pop();
                      }
                    }]
                  });
                  alert.present();
                  hpm.setAttribute('src', '../../assets/gifs/monster.gif');
                }, 1200);
              }
            }, 4100);
          }, 2000);
        } else {
          setTimeout(function () {
            that.logs.unshift(new Log(`Monstro causou dano (-${data["monsterAttack"]})`, "right", "danger"));
            that.hpwarrior = 0;
            hpw.setAttribute('src', '../../assets/gifs/dead.gif');
            hpm.setAttribute('src', '../../assets/gifs/monster.gif');
          }, 2000);
          setTimeout(function () {
            hpw.setAttribute('src', '../../assets/gifs/warrior.gif');
            const alert = that.alertCtrl.create({
              enableBackdropDismiss: false,
              title: 'Game Over!',
              message: 'O monstro conseguiu derrotá-lo. Tente novamente!',
              buttons: [{
                text: 'OK',
                handler: () => {
                  that.navCtrl.pop();
                }
              }]
            });
            alert.present();
          }, 3700);
        }
      }, err => {
        console.log(err);
      });
  }

  curar() {
    this.http.post(`${APIUrl}/play`, JSON.stringify({
      type: "cura",
      name: this.data.name
    }), {headers: {'Content-Type': 'application/json'}})
      .subscribe(data => {
        console.log(data);
        this.data = data;
        let hpm = document.getElementById("monstergif");
        let hpw = document.getElementById("warriorgif");
        hpm.setAttribute('src', "../../assets/gifs/mattack.gif");
        let that = this;
        if (data["life"] - data["cure"] > 0) {
          setTimeout(function () {
            if(data["monsterAttack"] > 0){
              that.logs.unshift(new Log(`Monstro causou dano (-${data["monsterAttack"]})`, "right", "danger"));
            } else {
              that.logs.unshift(new Log(`Monstro não causou dano`, "right", "danger"));
            }
            that.hpwarrior -= data["monsterAttack"];
            hpm.setAttribute('src', '../../assets/gifs/monster.gif');
            setTimeout(function () {
              hpw.setAttribute('src', '../../assets/gifs/recover.gif');
              that.logs.unshift(new Log(`Jogador usou a cura (+${data["cure"]})`, "left", "golden"));
              that.hpwarrior = data["life"];
            }, 1500);
            setTimeout(function () {
              hpw.setAttribute('src', '../../assets/gifs/warrior.gif');
            }, 2500);
            ;
          }, 1900);
        } else {
          setTimeout(function () {
            that.logs.unshift(new Log(`Monstro causou dano (-${data["monsterAttack"]})`, "right", "danger"));
            that.hpwarrior = 0;
            hpw.setAttribute('src', '../../assets/gifs/dead.gif');
            hpm.setAttribute('src', '../../assets/gifs/monster.gif');
          }, 2000);
          setTimeout(function () {
            hpw.setAttribute('src', '../../assets/gifs/warrior.gif');
            const alert = that.alertCtrl.create({
              enableBackdropDismiss: false,
              title: 'Game Over!',
              message: 'O monstro conseguiu derrotá-lo. Tente novamente!',
              buttons: [{
                text: 'OK',
                handler: () => {
                  that.navCtrl.pop();
                }
              }]
            });
            alert.present();
          }, 3700);
        }
      }, err => {
        console.log(err);
      });
  }

  desistir() {
    let hpm = document.getElementById("monstergif");
    let hpw = document.getElementById("warriorgif");
    hpm.setAttribute('src', "../../assets/gifs/mattack.gif");
    let that = this;
    setTimeout(function () {
      that.logs.unshift(new Log(`Monstro causou dano (-${that.hpwarrior})`, "right", "danger"));
      that.hpwarrior = 0;
      hpw.setAttribute('src', '../../assets/gifs/dead.gif');
    }, 1500);
    setTimeout(function () {
      that.navCtrl.pop();
    }, 2700);
  }

  getMsgColor(t: string) {
    switch (t) {
      case "danger":
        return "rgba(245, 61, 61, 0.51)";
      case "primary":
        return "rgba(72,138,255,0.51)";
      case "secondary":
        return "rgba(50, 219, 100, 0.51)";
      case "golden":
        return "rgba(255, 215, 0, 0.51)";
      default:
        return "rgba(0, 0, 0, 0.51)";
    }
  }

  getHpColor(n: number) {
    if (n > 80) {
      return "#86e01e";
    }
    if (n > 60) {
      return "#f2d31b";
    }
    if (n > 40) {
      return "#f2b01e";
    }
    if (n > 20) {
      return "#f27011";
    }
    return "#f63a0f";
  }
}
