import {Component} from '@angular/core';
import {NavController, ToastController} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import {GamePage} from "../game/game";
import {APIUrl} from "../../app/app.module";

@Component({
  selector: 'page-play',
  templateUrl: 'play.html'
})
export class PlayPage {
  name: String = "";

  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public http: HttpClient) {

  }

  createGame() {
    if (this.name == "") {
      const toast = this.toastCtrl.create({
        position: "middle",
        message: 'Por favor informe um nome!',
        duration: 2000
      });
      toast.present();
      return;
    } else{
      this.http.post(`${APIUrl}/game`, JSON.stringify({name: this.name}), {headers: {'Content-Type': 'application/json'}})
        .subscribe(data => {
          console.log(data);
          this.navCtrl.push(GamePage, {data: data});
        }, err => {
          console.log(err);
        });
    }
  }
}
