import { Component } from '@angular/core';
import {LoadingController, NavController} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import {APIUrl} from "../../app/app.module";

@Component({
  selector: 'page-ranking',
  templateUrl: 'ranking.html'
})
export class RankingPage {
  data: any;
  constructor(public navCtrl: NavController, public http: HttpClient, public loadingCtrl: LoadingController) {

  }

  ionViewDidEnter(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.http.get(`${APIUrl}/ranking`).subscribe(data => {
      this.data = data;
      loading.dismiss();
    }, err => {
      console.log(err);
    });
  }


}
